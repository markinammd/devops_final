import flask

app = flask.Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    return "<h1>Demo App</h1>"

@app.route('/calc')
def calc():
    a = int(flask.request.args.get('a'))
    b = int(flask.request.args.get('b'))
    return str(sum(a. b))

def sum(a, b):
    return a + b

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
